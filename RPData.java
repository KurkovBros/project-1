package test;

public class RPData {

    public static void main(String[] args) {
        ForecastRace forecast1 = new ForecastRace("29.03.17", "SEDGEFIELD", "2:10", "3m3f (3m3f9y) Betfred \"Racing's Biggest Supporter\" Handicap Hurdle (Class 5) (4yo+)", "3/1 Houndscourt, 5/1 Down Time, 6/1 Darsi Dancer, Onwiththeparty, Touch Of Steel, 7/1 Snapping Turtle, 9/1 Robins Legend, 10/1 Fresh By Nature, 25/1 Rocku, 40/1 Blue Cove", "Onwiththeparty", "5/1", "Down Time", "4/1J", "Blue Cove", "40/1", "Distances: 13l, 3l, 1?l", "Time: 6m 48.10s (slow by 28.10s)");

    }

    static class ForecastRace {

        public String date; // дата скачки
        public String huppo; // ипподром
        public String time; // время начала скачки
        public String type; // тип скачки
        public String forcasts; // прогнозы
        public String fave1; // первый фаворит
        public String k1; // коэффициент на победу первого фаворита
        public String fave2; // второй фаворит
        public String k2; // коэффициент на победу второго фаворита
        public String fave3; // третий фаворит
        public String k3; // коэффициент на победу третьего фаворита
        public String length; // отставание от победителя
        public String duration; // продолжительность скачки

        public ForecastRace(String s1, String s2, String s3, String s4, String s5, String s6, String s7, String s8, String s9, String s10, String s11, String s12, String s13) {
            date = s1;
            huppo = s2;
            time = s3;
            type = s4;
            forcasts = s5;
            fave1 = s6;
            k1 = s7;
            fave2 = s8;
            k2 = s9;
            fave3 = s10;
            k3 = s11;
            length = s12;
            duration = s13;
        }
    }
}
