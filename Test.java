package test;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class Test {
    
    // объявляем ArrayList объектов класса RPData
    static ArrayList<RPData> forecasts = new ArrayList<RPData>();

    public static void main(String[] args) throws IOException {
        for (int fileN = 1; fileN <= 14; fileN++) {
            String fileName = "/home/sasha/Работа/forecasts/" + fileN + ".txt";
            // вызываем функцию, которая будет читать файл с прогнозами и работать с ним
            work(fileName);
        }
        
        // вызываем функцию, которая будет проверять результат
        checkResult();
    }

    public static void work(String fileName) throws IOException {
        // вызываем метод BufferedReader
        File file = new File(fileName);
        FileReader fr = new FileReader(file);
        BufferedReader br = new BufferedReader(fr);
        String s1 = br.readLine();
        String s2 = br.readLine();
        String s3;
        while((s3 = br.readLine()) != null){
            String s4 = br.readLine();
            String s5 = br.readLine();
            String s6 = br.readLine();
            String s7 = br.readLine();
            String s8 = br.readLine();
            String s9 = br.readLine();
            String s10 = br.readLine();
            String s11 = br.readLine();
            String s12 = br.readLine();
            String s13 = br.readLine();
            RPData rpdata = new RPData(s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11, s12, s13);
            // добавляем сзоданный объект rpdata в ArrayList
            forecasts.add(rpdata);
        }
        br.close();
        fr.close();
        // обрезаем ArrayList, чтобы исключить утечку памяти
        forecasts.trimToSize();
    }
    
    public static void checkResult() {
        for(int i = 0; i < forecasts.size(); i++) {
            System.out.println(forecasts.get(i));
        }
    }
}
